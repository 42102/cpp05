
#include "../inc/Bureaucrat.hpp"
#include "../inc/AForm.hpp"
#include "../inc/ShrubberyCreationForm.hpp"
#include "../inc/RobotomyRequestForm.hpp"
#include "../inc/PresidentialPardonForm.hpp"
#include "../inc/Intern.hpp"
#include <iostream>

void test00(void);
void test01(void);
void test02(void);
void test03(void);

int main(void)
{
	test00();
	//test01();
	//test02();
	return 0;
}

void test00 (void)
{
    AForm *a;
	try
	{
        Bureaucrat b("julia", 40); 
		Intern i;
		a = i.makeForm("asdasd", "cualquiera");
		std::cout<<*a<<std::endl;
		/*
		b.signform(*a);
        a->execute(b);
        std::cout<<"form: "<<*a<<std::endl;
		*/
	}
	catch(std::exception& e)
	{
		std::cout<<"Error: "<<e.what()<<std::endl;
	}

	if(a != NULL)
		delete a;
}

