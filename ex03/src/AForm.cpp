
#include "../inc/AForm.hpp"

//Constructors and destructor
AForm::AForm (void)
    : _name("no_name"), _signed(false)
    , _sign_grade(150), _execute_grade(150)
{
    std::cout<<"AForm default constructor"<<std::endl;
}

AForm::AForm (AForm const &f)
{
    std::cout<<"AForm copy constructor"<<std::endl;
    *this = f;
}

AForm::AForm (std::string name, int sign_grade, int execute_grade)
    : _name(name), _signed(false)
{
    std::cout<<"AForm constructor with 3 parameters<string, int, int>"<<std::endl;
    if(sign_grade > 150)
        throw Bureaucrat::GradeTooLowException("Sign grade out of range!!!");
    if(execute_grade > 150)
        throw Bureaucrat::GradeTooLowException("Execute grade out of range!!!");
    if(sign_grade < 1)
        throw Bureaucrat::GradeTooHighException("Sign grade out of range!!!");
    if(execute_grade < 1)
        throw Bureaucrat::GradeTooHighException("Execute grade oug of range!!!");

    this->_sign_grade = sign_grade;
    this->_execute_grade = execute_grade;    
}

AForm::~AForm ()
{
    std::cout<<"Calling AForm destructor"<<std::endl;
}

//Operators
const AForm& AForm::operator= (AForm const &f)
{
    if(this != &f)
    {
        this->_name = f._name;
        this->_signed = f._signed;
        this->_sign_grade = f._sign_grade;
        this->_execute_grade = f._execute_grade;
    }

    return *this;
}

//Methods of AForm
const std::string& AForm::getName (void) const
{
    return this->_name;
}

bool AForm::getSigned (void) const
{
    return this->_signed;
}

int AForm::getSignGrade (void) const
{
    return this->_sign_grade;
}

int AForm::getExecuteGrade (void) const
{
    return this->_execute_grade;
}

void AForm::announce (std::ostream &out) const
{
    out<<"AForm = { name: "<<this->getName()<<", signed: "<<this->getSigned()
    <<", sign grade: "<<this->getSignGrade()<<", execute grade: "
    <<this->getExecuteGrade()<<" }";
}

bool AForm::beSigned (Bureaucrat const &b)
{
    if((int)b.getGrade() > this->getSignGrade())
        throw Bureaucrat::GradeTooLowException("Grade too low to be signed");
    if(this->_signed == true)
    {
        std::cout<<"AForm: "<<this->getName()<<" is already signed"<<std::endl;
        return false;
    }
    {
        this->_signed = true;
        return true;
    }
}
//----------------------------------------------------------------------------
//Internal class NotSignedException
//Constructors and destructor
AForm::NotSignedException::NotSignedException (void) 
    : _error_msg("NotSignedException: ")
{
}

AForm::NotSignedException::NotSignedException (std::string error_msg)
    : _error_msg("NotSignedException: " + error_msg)
{
}

AForm::NotSignedException::~NotSignedException () throw()
{
}

//Methods
const char* AForm::NotSignedException::what () const throw()
{
    return (this->_error_msg).c_str();
}
//----------------------------------------------------------------------------
//External methods
std::ostream& operator<< (std::ostream& out, AForm const &f)
{
    f.announce(out);
    return out;
}
