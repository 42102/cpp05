
#include "../inc/Intern.hpp"

//Class Intern
//Constructors and destructor

Intern::Intern (void)
{
	std::cout<<"Intern default constructor"<<std::endl;
}

Intern::Intern (Intern const &intern)
{
	std::cout<<"Intern copy constructor"<<std::endl;
	*this = intern;
}

Intern::~Intern ()
{
	std::cout<<"Calling Intern destructor"<<std::endl;
}

//Operators of Intern class

const Intern& Intern::operator= (Intern const &intern)
{
	(void) intern;
	return *this;
}

//Methods of class Intern

AForm* Intern::makeForm(std::string name, std::string target)
{

	unsigned int form = 0;
	std::string forms[3] = {"shrubbery creation", "robotomy request", 
		"presidential pardon"};
	AForm *listForm[3] = {new ShrubberyCreationForm(target), 
	new RobotomyRequestForm(target), new PresidentialPardonForm(target)};
	
	while(form < 3)
	{
		if(name.compare(forms[form]) == 0)
			break;
		form++;
	}

	if(form >= 3)
	{
		for(unsigned int i = 0; i < 3; i++)
		{
			delete listForm[i];
		}
		throw Intern::NotExistingForm(name + " not exists as form");
	}
	else
	{
		for(unsigned int i = 0; i < 3; i++)
		{
			if(i != form)
				delete listForm[i];
		}
	}

	
	return listForm[form];
}

//Class Error NotExistingForm 
//Constructors and destructor

Intern::NotExistingForm::NotExistingForm (void)
	: _error_msg("NotExistingForm")
{
}

Intern::NotExistingForm::NotExistingForm (std::string error_msg)
	: _error_msg("NotExistingForm: " + error_msg)
{

}

Intern::NotExistingForm::~NotExistingForm () throw()
{

}

//Methods of NotExistingForm

const char* Intern::NotExistingForm::what () const throw()
{
	return (this->_error_msg).c_str();
}
