
#include "../inc/ShrubberyCreationForm.hpp"

//Constructors and destructor of ShrubberyCreationForm
ShrubberyCreationForm::ShrubberyCreationForm (void)
    : AForm("ShrubberyCreationForm", 145, 137), _target("default_shrubbery")
{
    std::cout<<"ShrubberyCreationForm default constructor"<<std::endl;
}

ShrubberyCreationForm::ShrubberyCreationForm (ShrubberyCreationForm const & scf)
    :AForm(scf)
{
    std::cout<<"ShrubberyCreationForm copy constructor"<<std::endl;
   *this = scf; 
}

ShrubberyCreationForm::ShrubberyCreationForm (std::string target)
    : AForm("ShrubberyCreationForm", 145, 137), _target(target)
{
    std::cout<<"ShrubberyCreationForm constructor with 1 parameter<string>"<<std::endl;
}

ShrubberyCreationForm::~ShrubberyCreationForm ()
{
    std::cout<<"Calling ShrubberyCreationForm destructor"<<std::endl;
}

//Operators
const ShrubberyCreationForm& ShrubberyCreationForm::operator= (ShrubberyCreationForm const &scf)
{
    if(this != &scf)
    {
        AForm::operator= (scf);
        this->_target = scf._target;
    }

    return *this;
}
//Methods of ShrubberyCreationForm

bool ShrubberyCreationForm::execute (Bureaucrat const &executor) const
{
    if(this->getSigned() == false)
        throw AForm::NotSignedException(this->getName() + " is not signed");
    if((int)executor.getGrade() > this->getExecuteGrade())
        throw Bureaucrat::GradeTooLowException("Bureaucrat " + executor.getName() + 
               " cannot sign " + this->getName() + " because his grade is too low");
    
    if(!this->action())
    {
        std::cout<<this->_target<<"_shrubbery file could not have been created"<<std::endl;
        return false;
    }
    else
    {

        std::cout<<this->_target<<"_shrubbery file have been created"<<std::endl;
        return true;
    }
}

//Private methods of ShrubberyCreationForm
bool ShrubberyCreationForm::action (void) const
{
    const std::string tree[] = { 
        "    oxoxoo    ooxoo\n",
        "  ooxoxo oo  oxoxooo\n",
        " oooo xxoxoo ooo ooox\n",
        " oxo o oxoxo  xoxxoxo\n",
        "  oxo xooxoooo o ooo\n",
        "    ooo\\oo\\  /o/o\n",
        "        \\  \\/ /\n",
        "         |   /\n",
        "         |  |\n",
        "         | D|\n",
        "         |  |\n",
        "         |  |\n",
        "  ______/____\\____\n"
    };
    std::ofstream out;

    out.open((this->_target + "_shrubbery").c_str());

    if(out.fail())
    {
        std::cout<<"File open failed"<<std::endl;
        return false;
    }
    
    for(unsigned int i = 0; (i < 13) && (out.good()); i++)
    {
        out<<tree[i];
    }
    out.close();
    if(out.fail())
    {
        std::cout<<"File close failed"<<std::endl;
        return false;
    }
    return true;
}
