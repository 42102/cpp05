
#ifndef ROBOTOMY_REQUEST_FORM_HPP_
#define ROBOTOMY_REQUEST_FORM_HPP_

#include "AForm.hpp"
#include "Bureaucrat.hpp"
#include <iostream>
#include <ctime>
#include <cstdlib>

class RobotomyRequestForm: public AForm
{
	private:
		//Constants and variables
		std::string _target;
		
		//Private methods
		bool action(void) const;

	public:
		//Constructors and destructor
		RobotomyRequestForm (void);
		RobotomyRequestForm (RobotomyRequestForm const &rrf);
		RobotomyRequestForm (std::string target);
		~RobotomyRequestForm ();

		//Operators
		const RobotomyRequestForm& operator= (RobotomyRequestForm const &rrf);

		//Methods
		bool execute (Bureaucrat const & executor) const;
};
#endif
