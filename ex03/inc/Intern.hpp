
#ifndef INTERN_HPP_
#define INTERN_HPP_

#include "AForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
#include <iostream>

class Intern
{
	private:
			
	public:
		//Internal Error class
		class NotExistingForm: public std::exception
		{
			private:
				//Constants and variables
				std::string _error_msg;
				
			public:

				//Constructors and destructor
				NotExistingForm (void);
				NotExistingForm (std::string error_msg);
				~NotExistingForm () throw();

				const char * what() const throw();

		};

		//Constructors and destructor
		Intern (void);
		Intern (Intern const & intern);
		~Intern();
		
		//Operators
		const Intern& operator= (Intern const &intern);

		//Methods
		AForm* makeForm(std::string name, std::string target);	
};
#endif
