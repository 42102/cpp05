
#include "../inc/RobotomyRequestForm.hpp"

//Class RobotomyRequestForm
//Constructors and destructors

RobotomyRequestForm::RobotomyRequestForm (void)
	: AForm("RobotomyRequestForm", 72, 45), _target("default_robotomy")
{
	std::cout<<"RobotomyRequestForm default destructor"<<std::endl;
}

RobotomyRequestForm::RobotomyRequestForm (RobotomyRequestForm const &rrf)
	: AForm(rrf)
{
	std::cout<<"RobotomyRequestForm copy constructor"<<std::endl;
	*this = rrf;
}

RobotomyRequestForm::RobotomyRequestForm (std::string target)
	: AForm("RobotomyRequestForm", 72, 45), _target(target)
{
	std::cout<<"RobotomyRequestForm constructor with 1 parameter<string>"<<std::endl;
}
RobotomyRequestForm::~RobotomyRequestForm ()
{
	std::cout<<"Calling RobotomyRequestForm destructor"<<std::endl;
}

//Operators of RobotomyRequestForm

const RobotomyRequestForm& RobotomyRequestForm::operator= (RobotomyRequestForm const &rrf)
{
	if(this != &rrf)
	{
		AForm::operator= (rrf);
		this->_target = rrf._target;
	}

	return *this;
}

//Methods of RobotomyRequestForm
bool RobotomyRequestForm::execute (Bureaucrat const & executor) const
{
	if(!this->getSigned())
		throw AForm::NotSignedException (this->getName() + "is not signed");
	if((int)executor.getGrade() > this->getExecuteGrade())
		throw AForm::NotSignedException ("Bureaucrat " + executor.getName() + 
				" cannot sign " + this->getName() + "because his grade is too low");

	this->action();
	return true;
}

//Private methods of RobotomyRequestForm

bool RobotomyRequestForm::action (void) const
{
	unsigned int seed = std::time(NULL);
	unsigned int hits = 0;

	std::srand(seed);
	
	for(int i = 0; i < 9; i++)
	{	
		if((rand() % 100 ) >= 50)
			hits++;
	}

	if(hits > 4)
		std::cout<<this->_target<<" has been robotomized succesfully"<<std::endl;

	return true;
}
