
#include "../inc/Bureaucrat.hpp"
#include "../inc/AForm.hpp"
#include "../inc/ShrubberyCreationForm.hpp"
#include "../inc/RobotomyRequestForm.hpp"
#include "../inc/PresidentialPardonForm.hpp"
#include <iostream>

void test00(void);
void test01(void);
void test02(void);
void test03(void);

int main(void)
{
	//test00();
	//test01();
	test02();
	return 0;
}

void test00 (void)
{
    AForm *a;
	try{
        Bureaucrat b("Julia", 40);
		a = new ShrubberyCreationForm("prueba");
        b.signForm(*a);
        a->execute(b);
        std::cout<<"Form: "<<*a<<std::endl;
	}
	catch(std::exception& e)
	{
		std::cout<<"Error: "<<e.what()<<std::endl;
	}

	if(a != NULL)
		delete a;
}


void test01 (void)
{
    AForm *a;
	try{
        Bureaucrat b("Julia", 40);
		a = new RobotomyRequestForm("prueba");
        b.signForm(*a);
        a->execute(b);
        std::cout<<"Form: "<<*a<<std::endl;
	}
	catch(std::exception& e)
	{
		std::cout<<"Error: "<<e.what()<<std::endl;
	}

        if(a != NULL)
            delete a;
}


void test02 (void)
{
    AForm *a;
	try{
        Bureaucrat b("Julia", 42);
		a = new PresidentialPardonForm("prueba");
        b.signForm(*a);
        b.executeForm(*a);
        std::cout<<"Form: "<<*a<<std::endl;
	}
	catch(std::exception& e)
	{
		std::cout<<"Error: "<<e.what()<<std::endl;
	}

        if(a != NULL)
            delete a;
}
