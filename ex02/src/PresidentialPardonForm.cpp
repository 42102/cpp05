
#include "../inc/PresidentialPardonForm.hpp"

//Class PresidentialPardonForm
//Constructors and destructor

PresidentialPardonForm::PresidentialPardonForm (void)
	: AForm("PresidentialPardonForm", 25, 5), _target("default_presidential")
{
	std::cout<<"PresidentialPardonForm default constructor"<<std::endl;
}

PresidentialPardonForm::PresidentialPardonForm (PresidentialPardonForm const &ppf)
	:AForm(ppf)
{
	std::cout<<"PresidentialPardonForm copy constructor"<<std::endl;
	*this = ppf;
}

PresidentialPardonForm::PresidentialPardonForm (std::string target)
	: AForm("PresidentialPardonForm", 25, 5), _target(target)
{
	std::cout<<"PresidentialPardonForm constructor with 1 parameter<string>"<<std::endl;
}

PresidentialPardonForm::~PresidentialPardonForm ()
{
	std::cout<<"Calling PresidentialPardonForm destructor"<<std::endl;
}

//Operators of PresidentialPardonForm
const PresidentialPardonForm& PresidentialPardonForm::operator= (PresidentialPardonForm const &ppf)
{
	if(this != &ppf)
	{
		AForm::operator= (ppf);
		this->_target = ppf._target;
	}
	
	return *this;
}

//Methods of PresidentialPardonForm

bool PresidentialPardonForm::execute (Bureaucrat const &executor) const
{
    if(this->getSigned() == false)
        throw AForm::NotSignedException(this->getName() + " is not signed");
    if((int)executor.getGrade() > this->getExecuteGrade())
        throw Bureaucrat::GradeTooLowException("Bureaucrat " + executor.getName() + 
               " cannot execute " + this->getName() + " because his grade is too low");
    
	this->action();

	return true;
}

//Private methods of PresidentialPardonForm

bool PresidentialPardonForm::action (void) const
{
	std::cout<<this->_target<<" has been pardoned by Zaphod Beeblerox"<<std::endl;
	return true;
}
