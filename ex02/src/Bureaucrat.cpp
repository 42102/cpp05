
#include "../inc/Bureaucrat.hpp"

//Constructors and destructor
Bureaucrat::Bureaucrat(void)
	: _name("bureaucrat_empty"), _grade(150)
{
	std::cout<<"Bureaucrat default constructor"<<std::endl;
}

Bureaucrat::Bureaucrat(Bureaucrat const &b)
{
	std::cout<<"Bureaucrat copy constructor"<<std::endl;
	*this = b;
}

Bureaucrat::Bureaucrat (std::string name,int grade)
{
	std::cout<<"Bureaucrat constructor with 2 parameters<string, unsigned int>"<<std::endl;

	if(grade < 1)
		throw GradeTooHighException("Introduced grade is not valid!!!");
	if(grade > 150)
		throw GradeTooLowException("Introduced grade is not valid!!!");
	
	this->_grade = grade;
	this->_name = name;
}
Bureaucrat::~Bureaucrat()
{
	std::cout<<"Calling Bureaucrat destructor"<<std::endl;
}

//Operators

const Bureaucrat& Bureaucrat::operator= (Bureaucrat const &b)
{
	if(this != &b)
	{
		this->_name = b._name;
		this->_grade = b._grade;
	}

	return *this;
}

//Methods
const std::string& Bureaucrat::getName (void) const
{
	return this->_name;
}

unsigned int Bureaucrat::getGrade (void) const
{
	return this->_grade;
}

std::ostream& Bureaucrat::announce (std::ostream &out) const
{
	out<<this->_name<<", bureaucrat grade "<<this->_grade;
	return out;
}

bool Bureaucrat::increment(int inc)
{
	if(inc < 0)
	{
		std::cout<<"No se ha podido incrementar el grado, introduzca\n"
			<<"una cantidad positiva por favor"<<std::endl;
		return false;
	}
	else
	{
		(this->_grade) -= inc;

		if(this->_grade < 1)
			throw GradeTooHighException("Out of range");
		if(this->_grade > 150)
			throw GradeTooLowException("Out of range");
	}

	return true;
}

bool Bureaucrat::decrement(int dec)
{
	
	if(dec < 0)
	{
		std::cout<<"No se ha podido incrementar el grado, introduzca\n"
			<<"una cantidad positiva por favor"<<std::endl;
		return false;
	}
	else
	{
		(this->_grade) += dec;
		if(this->_grade < 1)
			throw GradeTooHighException("Out of range");
		if(this->_grade > 150)
			throw GradeTooLowException("Out of range");

	}

	return true;
}

bool Bureaucrat::signForm (AForm & f) const
{
	try{

		if(f.beSigned(*this) == true)
		{
			std::cout<<this->getName()<<" signed "<<f.getName()<<std::endl;
			return true;
		}
		else
		{
			std::cout<<this->getName()<<" could not sign "
			<<f.getName()<<" because was already signed"<<std::endl;
		}
	}catch(std::exception &e)
	{
		std::cout<<"Error: "<<e.what()<<std::endl;
		std::cout<<this->getName()<<"could not sign "
			<<f.getName()<<" because the bureaucrat grade was too low"<<std::endl;
	}

	return false;
}

bool Bureaucrat::executeForm (AForm const & form)
{
	try
	{
		if(form.execute(*this))
		{
			std::cout<<this->getName()<<" executed "<<form.getName()<<std::endl;
			return true;
		}
		else
		{
			std::cout<<this->getName()<<"could not execute "<<form.getName()
				<<std::endl;
			return false;
		}
	}
	catch(std::exception &e)
	{
		std::cout<<"Bureaucrat "<<this->getName()<<" had an error executing the form: "
			<<form.getName()<<std::endl;
		std::cout<<"Error: "<<e.what()<<std::endl;
		return false;
	}

}

//-----------------------------------------------------------------------------
//GradeTooHighException
//Constructors and destructor
Bureaucrat::GradeTooHighException::GradeTooHighException (void) throw()
	: _exception_msg("GradeTooHighException")
{
}

Bureaucrat::GradeTooHighException::GradeTooHighException (const std::string &msg) throw()
	: _exception_msg("GradeTooHighException: " + msg)
{
}

Bureaucrat::GradeTooHighException::~GradeTooHighException() throw()
{
}

//Methods

const char * Bureaucrat::GradeTooHighException::what() const throw()
{
	return (this->_exception_msg).c_str();
}


//-----------------------------------------------------------------------------
//GradeTooLowException
//Constructors and destructor

Bureaucrat::GradeTooLowException::GradeTooLowException (void) throw()
	: _exception_msg("GradeTooLowException")
{
}

Bureaucrat::GradeTooLowException::GradeTooLowException (std::string const &msg) throw()
	: _exception_msg("GradeTooLowException: " + msg)
{		
}

Bureaucrat::GradeTooLowException::~GradeTooLowException() throw()
{
}
//Methods
const char * Bureaucrat::GradeTooLowException::what () const throw()
{
	return (this->_exception_msg).c_str();
}

//-----------------------------------------------------------------------------
//External methods of file Bureaucrat.hpp
std::ostream& operator<< (std::ostream &out, Bureaucrat const &b)
{
	b.announce(out);
	return out;
}

