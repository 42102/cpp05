
#ifndef PRESIDENTIAL_PARDON_FORM_HPP_
#define PRESIDENTIAL_PARDON_FORM_HPP_

#include "AForm.hpp"
#include "Bureaucrat.hpp"
#include <iostream>

class PresidentialPardonForm: public AForm
{
	private:
		//Constants and variables
		std::string _target;

		//Private methods
		bool action(void) const;

	public:
		//Constructors and destructor
		PresidentialPardonForm (void);
		PresidentialPardonForm (PresidentialPardonForm const &ppf);
		PresidentialPardonForm (std::string target);
		~PresidentialPardonForm ();

		//Operators
		const PresidentialPardonForm& operator= (PresidentialPardonForm const &ppf);

		//Methods
		bool execute (Bureaucrat const & executor) const;
};

#endif
