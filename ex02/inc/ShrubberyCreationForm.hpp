
#ifndef _SHRUBBERY_CREATION_FORM_HPP_
#define _SHRUBBERY_CREATION_FORM_HPP_

#include "AForm.hpp"
#include <iostream>
#include <fstream>

class ShrubberyCreationForm: public AForm
{

    private:
        //Constants and variables
        std::string _target;

        //Private methods
        bool action(void) const;
    public:
        //Constructors and destructor
        ShrubberyCreationForm (void);
        ShrubberyCreationForm (ShrubberyCreationForm const &scf);
        ShrubberyCreationForm (std::string target);
        ~ShrubberyCreationForm ();

        //Operators
        const ShrubberyCreationForm& operator= (ShrubberyCreationForm const &scf);

        //Methods
        bool execute (Bureaucrat const &executor) const;
};
#endif
