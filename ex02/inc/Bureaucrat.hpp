
#ifndef BUREAUCRAT_HPP_
#define BUREAUCRAT_HPP_

class Bureaucrat;

#include "AForm.hpp"
#include <iostream>

class Bureaucrat
{
	private:
		//Members
		std::string _name;
		int _grade;

	public:
		//Exceptions
		class GradeTooHighException: public std::exception
		{
			private:
				std::string _exception_msg;
			
			public:
				//Constructors of GradeTooHighException
				GradeTooHighException(void) throw();
				GradeTooHighException(const std::string &msg) throw();
				~GradeTooHighException() throw();
					
				//Methods	
				const char * what() const throw();
		};

		class GradeTooLowException: public std::exception
		{
			private:
				std::string _exception_msg;

			public:
				//Constructors of GradeTooLowException
				GradeTooLowException(void) throw();
				GradeTooLowException(const std::string &msg) throw();
				~GradeTooLowException() throw();
				//Methods
				const char * what() const throw();
		};
		//Constructors and destructor
		Bureaucrat(void);
		Bureaucrat(Bureaucrat const &b);
		Bureaucrat(std::string name, int grade);
		~Bureaucrat();

		//Operators
		const Bureaucrat& operator= (Bureaucrat const &b);

		//Methods
		const std::string& getName (void) const;
		unsigned int getGrade (void) const;
		std::ostream& announce (std::ostream& out) const;
		bool increment(int inc);
		bool decrement(int dec);
		bool signForm (AForm  &f) const;
        bool executeForm (AForm const & form);
};

std::ostream& operator<<(std::ostream& out, Bureaucrat const &b);
#endif
