
#ifndef FORM_HPP_
#define FORM_HPP_

class AForm;
#include "./Bureaucrat.hpp"
#include <iostream>

class AForm
{
	private:
		std::string _name;
		bool _signed;
		int _sign_grade;
		int _execute_grade;

	public:
        //Internal classes for errors
        class NotSignedException: public std::exception
        {
            private:
                std::string _error_msg;

            public:
                NotSignedException(void);
                NotSignedException(std::string error_msg);
                virtual ~NotSignedException() throw();

                const char* what () const throw();

        };
		//Constructors and destructor
		AForm(void);
		AForm(AForm const &f);
		AForm(std::string name, int sign_grade, int execute_grade);
		virtual ~AForm();

		//Operators
		const AForm& operator= (AForm const &f);
		
		//Methods
		const std::string& getName (void) const;
		bool getSigned (void) const;
		int getSignGrade (void) const;
		int getExecuteGrade (void) const;

		void announce (std::ostream &out) const;
		bool beSigned (Bureaucrat const &b);

        virtual bool execute(Bureaucrat const &executor) const = 0;
};

std::ostream& operator<< (std::ostream &out, AForm const &f);

#endif
