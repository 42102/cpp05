
#include "../inc/Bureaucrat.hpp"
#include "../inc/Form.hpp"
#include <iostream>

void test00(void);
void test01(void);
void test02(void);
void test03(void);

int main(void)
{
	//test00();
	//test01();
	test02();
	return 0;
}

void test00 (void)
{

	try{
		Form f("28C", 0, 150);
	}
	catch(std::exception& e)
	{
		std::cout<<"Error: "<<e.what()<<std::endl;
	}
}

void test01 (void)
{

	try{
		Form f("28C", 23, 99);

		std::cout<<"Formulario: "<<f<<std::endl;
	}
	catch(std::exception& e)
	{
		std::cout<<"Error: "<<e.what()<<std::endl;
	}
}

void test02(void)
{

	try{
		Form f("28D", 150, 130);
		Bureaucrat b("Felipe", 149);
		b.signForm(f);

	}catch(std::exception &e)
	{
		std::cout<<"Error: "<<e.what()<<std::endl;
	}
}
