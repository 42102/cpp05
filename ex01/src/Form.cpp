
#include "../inc/Form.hpp"

//Constructors and destructor
Form::Form (void)
    : _name("no_name"), _signed(false)
    , _sign_grade(150), _execute_grade(150)
{
    std::cout<<"Form default constructor"<<std::endl;
}

Form::Form (Form const &f)
{
    std::cout<<"Form copy constructor"<<std::endl;
    *this = f;
}

Form::Form (std::string name, int sign_grade, int execute_grade)
    : _name(name), _signed(false)
{
    std::cout<<"Form constructor with 3 parameters<string, int, int>"<<std::endl;
    if(sign_grade > 150)
        throw Bureaucrat::GradeTooLowException("Sign grade out of range!!!");
    if(execute_grade > 150)
        throw Bureaucrat::GradeTooLowException("Execute grade out of range!!!");
    if(sign_grade < 1)
        throw Bureaucrat::GradeTooHighException("Sign grade out of range!!!");
    if(execute_grade < 1)
        throw Bureaucrat::GradeTooHighException("Execute grade oug of range!!!");

    this->_sign_grade = sign_grade;
    this->_execute_grade = execute_grade;    
}

Form::~Form ()
{
    std::cout<<"Calling Form destructor"<<std::endl;
}

//Operators
const Form& Form::operator= (Form const &f)
{
    if(this != &f)
    {
        this->_name = f._name;
        this->_signed = f._signed;
        this->_sign_grade = f._sign_grade;
        this->_execute_grade = f._execute_grade;
    }

    return *this;
}

//Methods of Form
const std::string& Form::getName (void) const
{
    return this->_name;
}

bool Form::getSigned (void) const
{
    return this->_signed;
}

int Form::getSignGrade (void) const
{
    return this->_sign_grade;
}

int Form::getExecuteGrade (void) const
{
    return this->_execute_grade;
}

void Form::announce (std::ostream &out) const
{
    out<<"Form = { name: "<<this->getName()<<", signed: "<<this->getSigned()
    <<", sign grade: "<<this->getSignGrade()<<", execute grade: "
    <<this->getExecuteGrade()<<" }";
}

bool Form::beSigned (Bureaucrat const &b)
{
    if((int)b.getGrade() > this->getSignGrade())
        throw Bureaucrat::GradeTooLowException("Grade too low to be signed");
    if(this->_signed == true)
    {
        std::cout<<"Form: "<<this->getName()<<" is already signed"<<std::endl;
        return false;
    }
    {

        this->_signed = true;
        return true;
    }
}
//External methods
std::ostream& operator<< (std::ostream& out, Form const &f)
{
    f.announce(out);
    return out;
}
