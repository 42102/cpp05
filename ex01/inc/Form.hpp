
#ifndef FORM_HPP_
#define FORM_HPP_

class Form;
#include "./Bureaucrat.hpp"
#include <iostream>

class Form
{
	private:
		std::string _name;
		bool _signed;
		int _sign_grade;
		int _execute_grade;

	public:
		//Constructors and destructor
		Form(void);
		Form(Form const &f);
		Form(std::string name, int sign_grade, int execute_grade);
		~Form();	

		//Operators
		const Form& operator= (Form const &f);
		
		//Methods
		const std::string& getName (void) const;
		bool getSigned (void) const;
		int getSignGrade (void) const;
		int getExecuteGrade (void) const;

		void announce (std::ostream &out) const;
		bool beSigned (Bureaucrat const &b);
};

std::ostream& operator<< (std::ostream &out, Form const &f);

#endif
