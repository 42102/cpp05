
#include "../inc/Bureaucrat.hpp"
#include <iostream>

void test00(void);
void test01(void);
void test02(void);
void test03(void);

int main(void)
{
	test00();
	test01();
	test02();
	test03();
	
	return 0;
}

void test00(void)
{	
	try
	{
		Bureaucrat b("Juan", 123);
		b.decrement(130);
		std::cout<<"descripcion de bureaucrat: "<<b<<std::endl;
	}catch(Bureaucrat::GradeTooHighException &e)
	{
		std::cout<<"Error: "<<e.what()<<std::endl;
	}
	catch(Bureaucrat::GradeTooLowException &e)
	{
		std::cout<<"Error: "<<e.what()<<std::endl;
	}
}


void test01(void)
{	
	try
	{
		Bureaucrat b("Juan", 123);
		b.increment(130);
		std::cout<<"descripcion de bureaucrat: "<<b<<std::endl;
	}catch(std::exception &e)
	{
		std::cout<<"Error: "<<e.what()<<std::endl;
	}
}


void test02(void)
{	
	try
	{
		Bureaucrat b("Juan", 155);
		b.increment(130);
		std::cout<<"descripcion de bureaucrat: "<<b<<std::endl;
	}catch(std::exception &e)
	{
		std::cout<<"Error: "<<e.what()<<std::endl;
	}
}


void test03(void)
{	
	try
	{
		Bureaucrat b("Juan", -23);
		b.increment(130);
		std::cout<<"descripcion de bureaucrat: "<<b<<std::endl;
	}catch(std::exception &e)
	{
		std::cout<<"Error: "<<e.what()<<std::endl;
	}
}
